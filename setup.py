from __future__ import with_statement

import os
import re
import subprocess
import sys
import warnings

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

from cerial.version import VERSION


def readme():
    try:
        with open('README.rst') as f:
            return f.read()
    except OSError:
        pass


install_requires = [
    'pycapnp >= 0.5.5, < 0.6.0',
    'typeannotations >= 0.1.0',
]

install_requires_py33 = [
    'enum34 >= 1.0',
    'pathlib >= 1.0',
]

tests_require = [
    'pytest >= 2.7.0',
]

docs_require = [
    'Sphinx >= 1.3',
]

extras_require = {
    ":python_version=='3.3'": install_requires_py33,
    'tests': tests_require,
    'docs': docs_require,
}

if sys.version_info < (3, 4):
    install_requires.extend(install_requires_py33)


if sys.platform == 'darwin':
    match = re.search(br'clang-([\d]+)',
                      subprocess.check_output(['cc', '--version']))
    if match and int(match.group(1)) >= 500 and \
       '-stdlib=libc++' not in os.environ.get('CFLAGS', ''):
        warnings.warn(
            'pycapnp package (which Serial depends on) might not be compiled '
            'on your system.  We recommend you to set '
            'CFLAGS="-stdlib=libc++ -mmacosx-version-min=10.7"'
        )


setup(
    name='Cerial',
    version=VERSION,
    description="Pythonic interface to Cap'n Proto",
    long_description=readme(),
    url='https://github.com/spoqa/cerial',
    author='Hong Minhee',
    author_email='hongminhee' '@' 'member.fsf.org',
    maintainer='Hong Minhee',
    maintainer_email='hongminhee' '@' 'spoqa.com',
    license='MIT License',
    packages=find_packages(exclude=['tests']),
    install_requires=install_requires,
    tests_require=tests_require,
    extras_require=extras_require,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: Stackless',
        'Topic :: Software Development :: Code Generators',
        'Topic :: Software Development :: Compilers',
        'Topic :: Software Development :: Object Brokering',
        'Topic :: Software Development :: Pre-processors',
        'Topic :: System :: Distributed Computing',
        'Topic :: System :: Networking',
    ]
)
