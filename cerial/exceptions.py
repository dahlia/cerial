""":mod:`cerial.exceptions` --- Exceptions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""

__all__ = 'CerialError', 'NotLoadedError', 'SchemaSpaceMismatchError'


class CerialError(Exception):
    """Base exception type for all errors caused by Cerial."""


class NotLoadedError(CerialError, LookupError, ValueError):
    """Requested Cap'n Proto node (e.g. type) is not loaded on memory."""


class SchemaSpaceMismatchError(CerialError):
    """:class:`~.schema.SchemaSpace` is not matched."""
