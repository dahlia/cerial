""":mod:`cerial.schema` --- Schema mappings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
import collections.abc
import enum
import weakref

from annotation.typed import optional, typechecked, union
from capnp import _DynamicStructReader, _StructModule
from capnp.lib.capnp import _EnumModule, _StructSchemaField

from .exceptions import NotLoadedError, SchemaSpaceMismatchError

__all__ = 'Field', 'SchemaSpace', 'Struct'


class SchemaSpace(collections.abc.MutableMapping):
    r"""Schema space is an isolated scope of loaded Cap'n Proto schemas.
    Even if two structs have the same id, if they are loaded by different
    :class;`SchemaSpace`\ s, they are incompatible.

    In most cases, only one :class:`SchemaSpace` is necessary for
    an application.  It's like :data:`sys.modules` of Python.

    """

    __slots__ = 'space', 'index', 'name'

    def __init__(self, name: optional(str)=None):
        self.space = weakref.WeakValueDictionary()
        self.index = weakref.WeakKeyDictionary()
        self.name = name

    @typechecked
    def dumps(self, obj, *, packed=False) -> bytes:
        """Make a Cap'n Proto message bytestring from the given ``obj``.

        :param obj: struct instance to serialize
        :type obj: :class:`Struct`
        :param packed: whether to use packed format.
                       :const:`False` by default.
                       keyword only argument
        :type packed: :class:`bool`
        :return: serialized Cap'n Proto message bytestring
        :rtype: :class:`bytes`

        """
        if not isinstance(obj, Struct):
            raise TypeError('expected an instance of {0.__module__}.'
                            '{0.__qualname__}, not {1!r}'.format(Struct, obj))
        elif obj.__schema_space__ is not self:
            raise SchemaSpaceMismatchError(
                "{0!r}'s schema space is not {1!r} but {2!r}".format(
                    obj, self, obj.__schema_space__
                )
            )
        m = obj.__capnp_message__
        return m.to_bytes_packed() if packed else m.to_bytes()

    @typechecked
    def loads(self, message: bytes, cls: type, *, packed=False):
        """Deserialize the given Cap'n Proto ``message`` bytestring.

        :param message: Cap'n Proto message bytestring to deserialize
        :type message: :class:`bytes`
        :param cls: the type of the given ``message``.
                    it has to be a subclass of :class:`Struct`
        :type cls: :class:`type`
        :param packed: whether to use packed format.
                       :const:`False` by default.
                       keyword only argument
        :type packed: :class:`bool`
        :return: deserialized object, an instance of ``cls``
        :rtype: :class:`Struct`

        """
        if not issubclass(cls, Struct):
            raise TypeError(
                '{0.__module__}.{0.__qualname__} is not a subclass of '
                '{1.__module__}.{1.__qualname__}'.format(cls, Struct)
            )
        elif cls.__schema_space__ is not self:
            raise SchemaSpaceMismatchError(
                "{0.__module__}.{0.__qualname__}'s schema space is not {1!r} "
                'but {2!r}'.format(cls, self, cls.__schema_space__)
            )
        struct = cls.__capnp_raw_struct__
        from_bytes = struct.from_bytes_packed if packed else struct.from_bytes
        message = from_bytes(message)
        instance = cls()
        instance.__capnp_message__ = message
        return instance

    @typechecked
    def get_name(self, raw: union(_StructModule, _EnumModule)) -> tuple:
        """Get the module name, basename and its qualified name of
        the given ``raw`` module.  Internal use only.

        :param raw: the raw module to get name
        :type raw: :class:`capnp._StructModule`,
                   :class:`capnp.lib.capnp._EnumModule`
        :return: a triple of ``(module, basename, qualname)``
        :rtype: :class:`tuple`

        """
        module = self.name or repr(self)
        _, qualname = raw._nodeProto.displayName.rsplit(':', 1)
        name = qualname[qualname.rfind('.') + 1:]
        return module, name, qualname

    @typechecked
    def get_struct(self, raw_struct: _StructModule) -> type:
        node_id = raw_struct._nodeProto.id
        if node_id in self:
            return self[node_id]
        module, name, qualname = self.get_name(raw_struct)
        fields = {
            name: Field(raw_field)
            for name, raw_field in raw_struct.schema.fields.items()
        }
        nested_nodes = {}
        for node in raw_struct._nodeProto.nestedNodes:
            node_attr = getattr(raw_struct, node.name)
            if isinstance(node_attr, _StructModule):
                nested_node = self.get_struct(node_attr)
            elif isinstance(node_attr, _EnumModule):
                nested_node = self.get_enum(node_attr)
            else:
                assert False, 'unsupported node: ' + repr(node_attr)
            nested_nodes[node.name] = nested_node
        attributes = {
            '__module__': module,
            '__capnp_id__': node_id,
            '__capnp_raw_struct__': raw_struct,
            '__capnp_fields__': tuple(fields.keys()),
            '__schema_space__': self,
        }
        attributes.update(fields)
        attributes.update(nested_nodes)
        cls = type(name, (Struct,), attributes)
        cls.__qualname__ = qualname
        self[node_id] = cls
        return cls

    @typechecked
    def get_enum(self, raw_enum: _EnumModule) -> enum.EnumMeta:
        node_id = raw_enum._nodeProto.id
        if node_id in self:
            return self[node_id]
        module, name, qualname = self.get_name(raw_enum)
        attributes = enum._EnumDict()
        enumerants = raw_enum.schema.enumerants
        attributes.update(enumerants)
        attributes.update(
            __module__=module,
            __qualname__=qualname,
            __capnp_id__=node_id,
            __capnp_raw_enum__=raw_enum,
            __schema_space__=self
        )
        attributes._member_names = list(enumerants.keys())
        cls = enum.EnumMeta(name, (enum.Enum,), attributes)
        self[node_id] = cls
        return cls

    @typechecked
    def encode_value(self, raw_type: _DynamicStructReader, value):
        """Encode a normal Python ``value`` into a raw value that pycapnp
        accepts.

        :param raw_type: the field slot type object of pycapnp
        :type raw_type: :class:`capnp._DynamicStructReader`
        :param value: a normal Python value to encode to raw value
        :return: raw value that pycapnp accepts

        """
        # TODO: anyPointer, interface, void
        typename = raw_type.which()
        if typename == 'enum':
            cls = self._get_by_type_id(raw_type.enum.typeId)
            if not isinstance(value, cls):
                raise TypeError('expected a {0.__module__}.{0.__qualname__}, '
                                'not {1!r}'.format(cls, value))
            return value.value
        elif typename == 'list':
            if not isinstance(value, collections.abc.Sequence):
                raise TypeError('expected a sequence, not ' + repr(value))
            encode_element = self.encode_value
            element_type = raw_type.list.elementType
            return [encode_element(element_type, el) for el in value]
        elif typename == 'struct':
            cls = self._get_by_type_id(raw_type.struct.typeId)
            if not isinstance(value, cls):
                raise TypeError('expected a {0.__module__}.{0.__qualname__}, '
                                'not {1!r}'.format(cls, value))
            return value.__capnp_message__
        elif typename == 'text':
            if not isinstance(value, str):
                raise TypeError('expected a str, not ' + repr(value))
            return value.encode('utf-8')
        return value

    @typechecked
    def decode_value(self, raw_type: _DynamicStructReader, raw_value):
        """Decode a ``raw_value`` that pycapnp uses into a corresponding
        normal Python value.

        :param raw_type: the field slot type object of pycapnp
        :type raw_type: :class:`capnp._DynamicStructReader`
        :param raw_value: a raw value that pycapnp uses
        :return: a corresponding normal Python value

        """
        # TODO: anyPointer, interface, void
        typename = raw_type.which()
        if typename == 'enum':
            cls = self._get_by_type_id(raw_type.enum.typeId)
            return cls(raw_value)
        elif typename == 'list':
            element_type = raw_type.list.elementType
            decode_element = self.decode_value
            return [decode_element(element_type, el) for el in raw_value]
        elif typename in ('struct', 'enum'):
            cls = self._get_by_type_id(raw_type.struct.typeId)
            value = cls()
            value.__capnp_message__ = raw_value
            return value
        return raw_value

    def _get_by_type_id(self, type_id: int) -> type:
        """Similar to ``__getitem__`` except it raises a better error
        message with proper exception type when there's no such type id.

        """
        try:
            return self[type_id]
        except KeyError:
            raise NotLoadedError(
                '0x{!x} is not loaded by {!r}'.format(type_id, self)
            )

    def __iter__(self):
        return iter(self.space)

    def __len__(self):
        return len(self.space)

    def __getitem__(self, key):
        return self.space[key]

    @typechecked
    def __setitem__(self, key: int, value: type):
        self.space[key] = value
        self.index[value] = key

    def __delitem__(self, key):
        value = self.space.pop(key)
        del self.index[value]

    def __repr__(self):
        return '{0.__module__}.{0.__qualname__}({1})'.format(
            type(self),
            '' if self.name is None else repr(self.name)
        )


class Field:
    r"""Descriptor for fields of :class:`Struct`\ s."""

    @typechecked
    def __init__(self, raw_field: _StructSchemaField):
        self.raw_field = raw_field

    @property
    def __capnp_field_name__(self):
        return self.raw_field.proto.name

    @property
    def __capnp_field_type__(self):
        return self.raw_field.proto.slot.type

    def __get__(self, obj, cls=None):
        if obj is None:
            return self
        raw_value = getattr(obj.__capnp_message__, self.__capnp_field_name__)
        schema_space = obj.__schema_space__
        return schema_space.decode_value(self.__capnp_field_type__, raw_value)

    def __set__(self, obj, value):
        schema_space = obj.__schema_space__
        raw_value = schema_space.encode_value(self.__capnp_field_type__, value)
        setattr(obj.__capnp_message__, self.__capnp_field_name__, raw_value)


class Struct:
    """Abstract base class of all Cap'n Proto structs.  The construct takes
    keyword only arguments that have the same name to fields.

    In addition, it also provides default implementations of the following
    some useful interfaces:

    - Value equality (:token:`=`, :token:`!=`)
    - Fine representation (:func:`repr()`)

    """

    def __init__(self, **fields):
        for name, value in fields.items():
            if name not in self.__capnp_fields__:
                raise TypeError(name + ' field does not exist')
        assert isinstance(self.__capnp_raw_struct__, _StructModule)
        self.__capnp_message__ = self.__capnp_raw_struct__.new_message()
        for field, value in fields.items():
            setattr(self, field, value)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return False
        for name in self.__capnp_fields__:
            if getattr(self, name) != getattr(other, name):
                return False
        return True

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        fields = [(f, getattr(self, f)) for f in self.__capnp_fields__]
        fields.sort(key=lambda pair: pair[0])
        return '{0.__module__}.{0.__qualname__}({1})'.format(
            type(self),
            ', '.join(map('{0[0]}={0[1]!r}'.format, fields))
        )
