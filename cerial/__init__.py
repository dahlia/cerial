""":mod:`cerial` --- Pythonic interface to `Cap'n Proto`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _Cap'n Proto: https://capnproto.org/

"""
try:
    import capnp
except ImportError:
    pass
else:
    capnp.remove_import_hook()

__all__ = ()
