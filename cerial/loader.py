""":mod:`cerial.loader` --- Schema loader
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
import collections.abc
import pathlib
import types

from annotation.typed import typechecked
from capnp import load as load_raw

__all__ = 'load',


@typechecked
def load(path: pathlib.Path, name: str,
         import_paths: collections.abs.Sequence=[]) -> types.ModuleType:
    """Load a Cap'n Proto schema language file (:file:`*.capnp`) and then
    return the compiled Python module.

    :param path: path to schema file
    :type path: :class:`pathlib.Path`
    :param name: the module name
    :type name: :class:`str`
    :param import_paths: list of import paths.  elements must be
                         :class:`pathlib.Path` instances instead of
                         :class:`str`

    :type import_paths: :class:`collections.abc.Sequence`
    :return: module compiled from the loaded schema
    :rtype: :class:`types.ModuleType`

    """
    if not path.exists():
        raise FileNotFoundError('{!s} does not exist'.format(path))
    elif path.is_dir():
        raise IsADirectoryError('{!s} is a directory'.format(path))
    imports = []
    for ipath in import_paths:
        if not isinstance(ipath, pathlib.Path):
            raise TypeError('import_paths must be a sequence of '
                            '{0.__module__}.{0.__qualname__}, but '
                            'it contains {1!r}'.format(pathlib.Path, ipath))
        elif ipath.exists():
            raise FileNotFoundError('{!s} does not exist'.format(ipath))
        elif not ipath.is_dir():
            raise NotADirectoryError('{!s} is not a directory'.format(ipath))
        imports.append(str(ipath))
    return load_raw(str(path), name, imports=imports)
