Cerial
======

Cerial is a Pythonic interface to `Cap'n Proto`_.

.. _Cap'n Proto: https://capnproto.org/


How Pythonic (compared to pycapnp_)
-----------------------------------

========================== =================================== ============
Cap'n Proto                Python                              Implemented?
========================== =================================== ============
``struct``                 normal class (``type``)             Implemented
field                      ``property``                        Implemented
``union``                  subclass                            Not yet
``enum``                   ``enum.Enum``                       Implemented
``group``                  inner class (``type``)              Not yet
``Bool``                   ``bool``                            Implemented
``{Int,UInt}{8,16,32,64}`` ``int``                             Implemented
``Float{32,64}``           ``float``                           Implemented
``Text``                   ``str``                             Implemented
``Data``                   ``bytes``                           Implemented
``List(T)``                ``collections.abc.MutableSequence`` Implemented
========================== =================================== ============

.. _pycapnp: https://github.com/jparyani/pycapnp


Author and license
------------------

Cerial is written by `Hong Minhee`_, maintained by Spoqa_, and distributed
under MIT license.

.. _Hong Minhee: http://hongminhee.org/
.. _Spoqa: http://spoqa.com/
