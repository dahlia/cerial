.. Cerial documentation master file, created by
   sphinx-quickstart on Wed Apr 15 14:24:31 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Cerial
======

Cerial is a Pythonic interface to `Cap'n Proto`_.

.. _Cap'n Proto: https://capnproto.org/


How Pythonic (compared to pycapnp_)
-----------------------------------

========================== ======================================== ============
Cap'n Proto                Python                                   Implemented?
========================== ======================================== ============
``struct``                 normal class (:class:`type`)             Implemented
field                      :class:`property`                        Implemented
``union``                  subclass                                 Not yet
``enum``                   :class:`enum.Enum`                       Implemented
``group``                  inner class (:class:`type`)              Not yet
``Bool``                   :class:`bool`                            Implemented
``{Int,UInt}{8,16,32,64}`` :class:`int`                             Implemented
``Float{32,64}``           :class:`float`                           Implemented
``Text``                   :class:`str`                             Implemented
``Data``                   :class:`bytes`                           Implemented
``List(T)``                :class:`collections.abc.MutableSequence` Implemented
========================== ======================================== ============


API
---

.. _pycapnp: https://github.com/jparyani/pycapnp

.. toctree::
   :maxdepth: 2

   cerial


Indices and tables
------------------

- :ref:`genindex`
- :ref:`modindex`
- :ref:`search`


Author and license
------------------

Cerial is written by `Hong Minhee`_, maintained by Spoqa_, and distributed
under MIT license.

.. _Hong Minhee: http://hongminhee.org/
.. _Spoqa: http://spoqa.com/
