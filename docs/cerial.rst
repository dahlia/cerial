
.. automodule:: cerial

   .. toctree::
      :maxdepth: 2

      cerial/exceptions
      cerial/schema
      cerial/version
