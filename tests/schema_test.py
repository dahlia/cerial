import collections.abc
import enum
import os.path

from capnp import load
from pytest import mark, raises

from cerial.schema import Field, SchemaSpace, Struct


test_schema_path = os.path.join(os.path.dirname(__file__), 'test_schema.capnp')
test_schema = load(test_schema_path)


def test_schema_space_mapping():
    """SchemaSpace should implement mapping protocol."""
    space = SchemaSpace()
    assert isinstance(space, collections.abc.MutableMapping)
    fake_struct_cls = type('Fake', (Struct,), {
        '__capnp_id__': 0xaebe5f0540cbcf45,
        '__capnp_raw_struct__': None,  # fake
        '__capnp_fields__': (),  # fake
        '__schema_space__': space,
        })
    assert len(space) == 0
    assert list(space) == []
    # __getitem__() raises KeyError if there's no such node id
    with raises(KeyError):
        space[fake_struct_cls.__capnp_id__]
    # There's no index as well if it's never set
    with raises(KeyError):
        space.index[fake_struct_cls]
    # __setitem__()
    space[fake_struct_cls.__capnp_id__] = fake_struct_cls
    assert len(space) == 1
    assert list(space) == [fake_struct_cls.__capnp_id__]
    # __setitem__() also sets index
    assert space.index[fake_struct_cls] == fake_struct_cls.__capnp_id__
    # __delitem__() removes the key and its corresponding index
    del space[fake_struct_cls.__capnp_id__]
    assert fake_struct_cls.__capnp_id__ not in space
    assert fake_struct_cls not in space.index
    assert len(space) == 0
    assert list(space) == []


def test_schema_space_get_name():
    space = SchemaSpace(name='mod.name')
    triple = space.get_name(test_schema.Person)
    assert triple == ('mod.name', 'Person', 'Person')
    triple = space.get_name(test_schema.Date)
    assert triple == ('mod.name', 'Date', 'Date')
    triple = space.get_name(test_schema.Person.PhoneNumber)
    assert triple == ('mod.name', 'PhoneNumber', 'Person.PhoneNumber')
    triple = space.get_name(test_schema.Person.PhoneNumber.Type)
    assert triple == ('mod.name', 'Type', 'Person.PhoneNumber.Type')


def test_schema_space_get_struct():
    space = SchemaSpace()
    person_cls = space.get_struct(test_schema.Person)
    date_cls = space.get_struct(test_schema.Date)
    # All structs have to be a normal class object
    assert isinstance(person_cls, type)
    assert isinstance(date_cls, type)
    assert isinstance(person_cls.PhoneNumber, type)
    # All structs have to be an instance of Struct
    assert issubclass(person_cls, Struct)
    assert issubclass(date_cls, Struct)
    assert issubclass(person_cls.PhoneNumber, Struct)
    # Cap'n Proto node id has to be preserved
    assert person_cls.__capnp_id__ == 0xed5bcc458b243f52
    assert date_cls.__capnp_id__ == 0xef29c66fa74a8c93
    assert person_cls.PhoneNumber.__capnp_id__ == 0xd68b5724fed51061
    # Fields have to be an instance of Field, which is a descriptor
    assert isinstance(person_cls.name, Field)
    assert isinstance(person_cls.birthdate, Field)
    assert isinstance(person_cls.email, Field)
    assert isinstance(person_cls.phones, Field)
    assert isinstance(person_cls.PhoneNumber.number, Field)
    assert isinstance(person_cls.PhoneNumber.type, Field)
    assert isinstance(date_cls.year, Field)
    assert isinstance(date_cls.month, Field)
    assert isinstance(date_cls.day, Field)
    # pycapnp raw node has to be preserved
    assert person_cls.__capnp_raw_struct__ is test_schema.Person
    assert date_cls.__capnp_raw_struct__ is test_schema.Date
    # SchemaSpace instance has to be preserved
    assert person_cls.__schema_space__ is space
    assert date_cls.__schema_space__ is space
    assert person_cls.PhoneNumber.__schema_space__ is space
    # Identity mapping
    assert space.get_struct(test_schema.Person) is person_cls
    assert space.get_struct(test_schema.Date) is date_cls


def test_schema_space_get_enum():
    space = SchemaSpace()
    raw_enum = test_schema.Person.PhoneNumber.Type
    enum_cls = space.get_enum(raw_enum)
    # All structs have to be a normal class object
    assert isinstance(enum_cls, type)
    # It has to be a normal enum.Enum type
    assert issubclass(enum_cls, enum.Enum)
    assert dict(enum_cls.__members__) == {
        'mobile': enum_cls.mobile,
        'home': enum_cls.home,
        'work': enum_cls.work,
    }
    assert enum_cls.mobile == enum_cls(0)
    assert enum_cls.home == enum_cls(1)
    assert enum_cls.work == enum_cls(2)
    # Cap'n Proto node id has to be preserved
    assert enum_cls.__capnp_id__ == 0xe1432335ec44693f
    # pycapnp raw node has to be preserved
    assert enum_cls.__capnp_raw_enum__ is raw_enum
    # SchemaSpace instance has to be preserved
    assert enum_cls.__schema_space__ is space
    # Identity mapping
    assert space.get_enum(test_schema.Person.PhoneNumber.Type) is enum_cls


def test_struct():
    space = SchemaSpace()
    person_cls = space.get_struct(test_schema.Person)
    date_cls = space.get_struct(test_schema.Date)
    phone_1 = person_cls.PhoneNumber(
        number='+82 10-1234-5678',
        type=person_cls.PhoneNumber.Type.mobile
    )
    phone_2 = person_cls.PhoneNumber(
        number='+82 2-1234-5678',
        type=person_cls.PhoneNumber.Type.work
    )
    person = person_cls(
        name='Hong Minhee',
        birthdate=date_cls(year=1988, month=8, day=4),
        email='hongminhee' '@' 'member.fsf.org',
        phones=[phone_1, phone_2]
    )
    assert isinstance(person, person_cls)
    assert person.name == 'Hong Minhee'
    assert person.birthdate == date_cls(year=1988, month=8, day=4)
    assert person.email == 'hongminhee' '@' 'member.fsf.org'
    assert isinstance(person.phones, collections.abc.Sequence)
    assert list(person.phones) == [phone_1, phone_2]
    assert phone_1.type is person_cls.PhoneNumber.Type.mobile
    assert phone_2.type is person_cls.PhoneNumber.Type.work


def test_struct_eq():
    space = SchemaSpace()
    date_cls = space.get_struct(test_schema.Date)
    a = date_cls(year=1988, month=8, day=4)
    b = date_cls(year=1988, month=8, day=4)
    c = date_cls(year=1989, month=7, day=9)
    assert a == b
    assert not (a != b)
    assert a != c
    assert not (a == c)
    person_cls = space.get_struct(test_schema.Person)
    d = person_cls(
        name='Hong Minhee',
        birthdate=a,
        email='hongminhee' '@' 'member.fsf.org',
        phones=[]
    )
    e = person_cls(
        name='Hong Minhee',
        birthdate=a,
        email='hongminhee' '@' 'member.fsf.org',
        phones=[]
    )
    f = person_cls(
        name='Hong Minhee',
        birthdate=c,
        email='hongminhee' '@' 'member.fsf.org',
        phones=[]
    )
    assert d == e
    assert not (d != e)
    assert d != f
    assert not (d == f)


@mark.parametrize('packed', [False, True])
def test_dumps(packed):
    space = SchemaSpace()
    person_cls = space.get_struct(test_schema.Person)
    date_cls = space.get_struct(test_schema.Date)
    p = person_cls(
        name='Hong Minhee',
        birthdate=date_cls(year=1988, month=8, day=4),
        email='hongminhee' '@' 'member.fsf.org',
        phones=[]
    )
    dumped = space.dumps(p, packed=packed)
    person_struct = test_schema.Person
    if packed:
        message = person_struct.from_bytes_packed(dumped)
    else:
        message = person_struct.from_bytes(dumped)
    assert message.name == 'Hong Minhee'
    dob_message = message.birthdate
    assert dob_message.year == 1988
    assert dob_message.month == 8
    assert dob_message.day == 4
    assert message.email == 'hongminhee' '@' 'member.fsf.org'
    assert len(message.phones) == 0


@mark.parametrize('packed', [False, True])
def test_loads(packed):
    space = SchemaSpace()
    person_cls = space.get_struct(test_schema.Person)
    date_cls = space.get_struct(test_schema.Date)
    message = test_schema.Person.new_message(
        name='Hong Minhee',
        birthdate=test_schema.Date.new_message(year=1988, month=8, day=4),
        email='hongminhee' '@' 'member.fsf.org',
        phones=[]
    )
    loaded = space.loads(
        message.to_bytes_packed() if packed else message.to_bytes(),
        person_cls,
        packed=packed
    )
    assert loaded == person_cls(
        name='Hong Minhee',
        birthdate=date_cls(year=1988, month=8, day=4),
        email='hongminhee' '@' 'member.fsf.org',
        phones=[]
    )


def test_struct_repr():
    space = SchemaSpace('t')
    person_cls = space.get_struct(test_schema.Person)
    date_cls = space.get_struct(test_schema.Date)
    p = person_cls(
        name='Hong Minhee',
        birthdate=date_cls(year=1988, month=8, day=4),
        email='hongminhee' '@' 'member.fsf.org',
        phones=[]
    )
    expected = (
        "t.Person(birthdate=t.Date(day=4, month=8, year=1988), "
        "email='hongminhee" "@" "member.fsf.org', name='Hong Minhee', "
        "phones=[])"
    )
    assert repr(p) == expected
